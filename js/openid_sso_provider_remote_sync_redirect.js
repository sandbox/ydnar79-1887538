(function ($) {
/* Show a modal message when user clicks on "Synchronize now" (there may be a wait). */

Drupal.behaviors.openid_sso_provider_remote_sync_redirect = {};
Drupal.behaviors.openid_sso_provider_remote_sync_redirect.attach = function(context) {

var uid = Drupal.settings.openid_sso_provider_remote_sync_colorbox_uid;

        // Attach to remote sync links. This falls back gracefully without JS.
      $("a[href*='/user/" + uid + "/network-sites'], a[href*='?q=user/" + uid + "/network-sites']", context).click(function() {
    $.blockUI({ css: {border: 'none', background: 'transparent', color: '#CCCCCC'}, message:  Drupal.settings.openid_sso_provider_remote_sync_wait_message});
  });
}

})(jQuery);
