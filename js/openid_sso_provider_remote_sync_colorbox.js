(function ($) {
  Drupal.behaviors.openid_sso_provider_remote_sync_colorbox = {
    attach: function(context, settings) {
      if (!$.isFunction($.colorbox)) {  // Check if colorbox is available.
        return;
      }

          var uid = Drupal.settings.openid_sso_provider_remote_sync_colorbox.uid;

        // Attach colorbox to remote sync links. This falls back gracefully without JS.
      $("a[href*='/user/" + uid + "/network-sites/sync'], a[href*='?q=user/" + uid + "/network-sites/sync']", context).each(function() {
          var path = this.href;
          var new_path = path.replace('user/' + uid + '/network-sites/sync','user/' + uid + '/network-sites/colorbox/sync'); // Replace the standard path.

          this.href = new_path;

      $(this).colorbox({
          speed: 0,
          iframe: true,
          width: '0px',
          height: '0px',
          overlayClose: false,
          escKey: false,
          opacity: 0

        });
      });
    }
  };
})(jQuery);
