<?php

/**
 * @file
 * Views wizard for relying party views.
 */

$plugin = array(
  'name' => 'relying_party',
  'base_table' => 'openid_sso_provider_rps',
//  'created_column' => 'created',
  'available_sorts' => array(
    'label:DESC' => t('Label')
  ),
  'form_wizard_class' => array(
    'file' => 'openid_sso_provider_views_rps_views_wizard.class.php',
    'class' => 'OpenidSsoProviderViewsRpsViewsWizard',
  ),
  'title' => t('Relying parties'),
  'path_field' => array(
    'id' => 'rpsid',
    'table' => 'node',
    'field' => 'rpsid',
    'exclude' => TRUE,
    'link_to_realm' => FALSE,
    'alter' => array(
      'alter_text' => 1,
      'text' => '[realm]',
    ),
  ),
);


// @TODO: REMOVE: http://drupal.org/node/1299464
