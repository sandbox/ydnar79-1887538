<?php

/**
 * Generates the relying party editing entity form.
 */
function openid_sso_provider_rps_form($form, &$form_state, $entity_type, $op = 'edit', $schnurps) {
  $options = array();

  if ($op == 'clone') {
    $entity_type->label .= ' (cloned)';
    $entity_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $entity_type->label,
    '#description' => t('The human-readable name of the relying party.'),
    '#required' => TRUE,
    '#size' => 30,
  );
  if (isset($rp)) {
    $form['realm'] = array(
      '#type' => 'hidden',
      '#default_value' => empty($rp) ? '' : $rp->realm,
    );
  }
  else {
    $form['realm'] = array(
      '#type' => 'textfield',
      '#title' => t('Realm (URL)'),
      '#description' => t('The URL of a trusted relying party.'),
      '#default_value' => empty($rp) ? '' : $rp->realm,
      '#required' => TRUE,
    );
  }
  $form['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('ENABLE site'),
    '#description' => t('A site MUST be ENABLED for it to be utilized.
      By disabling a site you temporarily remove it from the users\' lists as well as prevent SSO from this site.
      <br> NOTE: This can be quite useful when performing maintenance or for any other time in which you want to temporarily\
      disable a particular site.'),
    '#default_value' =>  empty($rp) ? 1 : $rp->enabled,
  );

  $form['extended_network'] = array(
    '#type' => 'checkbox',
    '#title' => t('This site is part of the EXTENDED NETWORK'),
    '#description' => t('This site is part of the "EXTENDED NETWORK" of sites and not a part of the core network.
      <br> This is useful for instances where your company owns a group of sites that you want to have classified as your
      "core" network.  But you may also want to allow associates/affiliates/partners/etc who have sites included as part of the "extended" network.
      This option allows you to differentiate between the two.
      <br> There are only MINOR differences between the two.  Under the user\'s account they will have a tab for NETWORK SITES (if they have permission) that will list both the CORE network and EXTENDED network
      sites.  They will be able to UNASSOCIATE the EXTENDED network sites from their account.  (Just like regular OpenID sites.)
      But they will not have this option available to them for CORE network sites since that would prevent them from logging in.  (Due to the SSO)'),
    '#default_value' => empty($rp) ? 0 : $rp->extended_network,
  );
  $form['automatic'] = array(
    '#type' => 'checkbox',
    '#title' => t('Connect automatically without asking the user during login.
      <br> NOTE: CORE NETWORK sites will do this anyways, but you do have the option to set this on EXTENDED NETWORK sites as well.'),
    '#default_value' =>  empty($rp) ? 0 : $rp->automatic,
    '#states' => array(
      // Only show this field when the 'extended network' checkbox is enabled.
      'visible' => array(
        ':input[name="extended_network"]' => array('checked' => TRUE),
      ),
    ),
  );

    $form['sync'] = array(
      '#type' => 'fieldset',
      '#title' => t('Remote Synchronization'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#states' => array(
        // Only show this field when this is a CORE NETWORK site.
        'visible' => array(
          ':input[name="extended_network"]' => array('checked' => FALSE),
        ),
      ),
    );
    $form['sync']['remote_sync'] = array(
    '#type' => 'checkbox',
    '#title' => t('Activate REMOTE Synchronization with this site.'),
    '#description' => t('This is available to CORE NETWORK sites ONLY.  This allows for the user to have to option
      of forcing a synchronization between the OpenID SSO Provider site and this network site.  When enabled, an additional
      link will appear on their network sites list.
      <br> IMPORTANT: For this to properly, the settings MUST be the same between this site and the RELYING PARTY site.
      If they are not, this will NOT work correctly and will negatively impact the user\'s experience.
      <br> NOTE: If the Colorbox module is installed, it will automatically be used to improve the user\'s experience.'),
    '#default_value' => empty($rp) ? 0 : $rp->remote_sync,
  );

    $form['sync']['remote_sync_minimum_time'] = array(
      '#type' => 'textfield',
      '#title' => t('MINIMUM time between Synchronizations'),
      '#description' => t('The MINIMUM time in SECONDS between synchronizations.
        <br> IMPORTANT:  This MUST be the same as the Relying Party website settings for this to work correctly.'),
      '#default_value' => empty($rp) ? 0 : $rp->remote_sync_minimum_time,
      '#required' => TRUE,
      '#states' => array(
        'visible' => array(
          ':input[name="remote_sync"]' => array('checked' => TRUE),
        ),
      ),
    );

    $form['sync']['api_key'] = array(
      '#type' => 'textfield',
      '#title' => t('API Key'),
      '#description' => t('The API Key set on the Relying Party website.
        <br> IMPORTANT: If NOT set correctly, synchronization WILL NOT occur.'),
      '#default_value' => empty($rp) ? '' : $rp->api_key,
      '#states' => array(
        'visible' => array(
          ':input[name="remote_sync"]' => array('checked' => TRUE),
        ),
      ),
    );

$form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save relying party'),
    '#weight' => 40,
  );

  if (!$entity_type->isLocked() && $op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete relying party'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('openid_sso_provider_rps_form_submit_delete')
    );
  }
  return $form;
}

/**
 * Validation for openid_sso_provider_rp_edit_form().
 */
function openid_sso_provider_rp_edit_form_validate($form, &$form_state) {

  if (!is_numeric($form_state['values']['remote_sync_minimum_time'])) {
      form_set_error('remote_sync_minimum_time', t('MINIMUM time must be numeric.'));
    }
  if (intval($form_state['values']['remote_sync_minimum_time']) < 0) {
      form_set_error('remote_sync_minimum_time', t('MINIMUM Time Limit between Synchronizations must be ZERO OR GREATER.'));
    }
}

/**
 * Submit handler for openid_sso_provider_rp_edit_form().
 */
function openid_sso_provider_rp_edit_form_submit($form, &$form_state) {
  openid_sso_provider_rp_add(trim($form_state['values']['realm'], '/') . '/', $form_state['values']['name'], 
      $form_state['values']['enabled'], $form_state['values']['extended_network'], $form_state['values']['automatic'],
      $form_state['values']['remote_sync'], $form_state['values']['remote_sync_minimum_time'] , $form_state['values']['api_key']);
  drupal_set_message(t('Added Relying Party @realm (@name).', array('@realm' => $form_state['values']['realm'], '@name' => $form_state['values']['name'])));
  $form_state['redirect'] = 'admin/config/people/openid-sso-provider';
}

/**
 * Remove form callback.
 */
function openid_sso_provider_rp_remove_form($form, $form_state) {
  $rp = openid_sso_provider_relying_party($_GET['realm']);
  if ($rp) {
    $form = array();
    $form['#realm'] = $rp->realm;
    $question = t('Remove Relying Party?');
    $description = t('If you remove the Relying Party @realm (@name), it cannot use this OpenID Provider site for authentication anymore. You can add it back at any point later.', array('@realm' => $rp->realm, '@name' => $rp->name));
    return confirm_form($form, $question, 'admin/config/people/openid-sso-provider', $description, t('Remove'));
  }
}

/**
 * Entity submit handler; Relying party.
 */
function openid_sso_provider_rps_form_submit($form, &$form_state) {

  // Save entity of relying party.
  $entity = $form_state['openid_sso_provider_rps'];
  entity_form_submit_build_entity('openid_sso_provider_rps', $entity, $form, $form_state);
  openid_sso_provider_rps_save($entity);

  // Save changed trusted settings on relyin party.
  if ($form_state['values']['trusted'] != $form_state['values']['trusted_before']) {
    openid_sso_provider_set_trust_definition($form_state['values']['realm'], $form_state['values']['trusted']);
  }

  $form_state['redirect'] = 'admin/config/services/openid-sso-provider/relying-parties';
  drupal_set_message(t('Relying party has been saved.'));
}

/**
 * Disable form callback.
 */
function openid_sso_provider_rp_disable_form($form, $form_state) {
  $rp = openid_sso_provider_relying_party($_GET['realm']);
  if ($rp) {
    $form = array();
    $form['#realm'] = $rp->realm;
    $question = t('Disable Relying Party?');
    $description = t('If you disable the Relying Party @realm (@name), it cannot use this OpenID Provider site for authentication anymore. You can enable it at any point later.', array('@realm' => $rp->realm, '@name' => $rp->name));
    return confirm_form($form, $question, 'admin/config/people/openid-sso-provider', $description, t('Disable'));
  }
}

/**
 * Disable form submit handler.
 */
function openid_sso_provider_rp_disable_form_submit($form, &$form_state) {
  openid_sso_provider_rp_disable($form['#realm']);
  $form_state['redirect'] = 'admin/config/people/openid-sso-provider';
}


/**
 * Disable form callback.
 */
function openid_sso_provider_rp_enable_form($form, $form_state) {
  $rp = openid_sso_provider_relying_party($_GET['realm']);
  if ($rp) {
    $form = array();
    $form['#realm'] = $rp->realm;
    $question = t('Enable Relying Party?');
    $description = t('If you enable the Relying Party @realm (@name), it CAN use this OpenID Provider site for authentication. You can disable it at any point later.', array('@realm' => $rp->realm, '@name' => $rp->name));
    return confirm_form($form, $question, 'admin/config/people/openid-sso-provider', $description, t('Enable'));
  }
}

/**
 * Disable form submit handler.
 */
function openid_sso_provider_rp_enable_form_submit($form, &$form_state) {
  openid_sso_provider_rp_enable($form['#realm']);
  $form_state['redirect'] = 'admin/config/people/openid-sso-provider';
}
/**
 * Admin settings page callback.
 */
function openid_sso_provider_rps_form_submit_delete($form, &$form_state) {
  $entity = $form_state['openid_sso_provider_rps'];
  $form_state['redirect'] = 'admin/config/services/openid-sso-provider/relying-parties/manage/' . $entity->machine_name . '/delete';
}

  $rows = array();
  foreach ($rps as $rp) {
    if ($rp->enabled == 1) { // Site is currently ACTIVE.
      $disable = l(t('disable'), 'admin/config/people/openid-sso-provider/disable', array('query' => array('realm' => urlencode($rp->realm))));
    } else {  // Site is currently DISABLED.
      $enable = l(t('enable'), 'admin/config/people/openid-sso-provider/enable', array('query' => array('realm' => urlencode($rp->realm))));
    }
    $edit = l(t('edit'), 'admin/config/people/openid-sso-provider/edit', array('query' => array('realm' => urlencode($rp->realm))));
    $remove = l(t('remove'), 'admin/config/people/openid-sso-provider/remove', array('query' => array('realm' => urlencode($rp->realm))));
    $network_type = $rp->extended_network == 0 ? 'CORE NETWORK' : 'EXTENDED NETWORK';

    if ($rp->enabled == 1) { // Site is currently ACTIVE.
      $rows[] = array(
        l(check_plain($rp->realm), $rp->realm),
        check_plain($rp->name),
        $network_type,
        $disable . ' | ' . $edit . ' | ' . $remove,
      );
    } else {  // Site is currently DISABLED.
      $rows[] = array(
        l(check_plain($rp->realm), $rp->realm),
        check_plain($rp->name) . ' <span class="admin-disabled">(disabled)</span>',
        $network_type,
        $enable . ' | ' . $edit . ' | ' . $remove,
      );
    }

  }

  return theme('table', array('header' => array(t('Realm'), t('Name'), array('data' => t('Type of Site'), 'colspan' => 3)),
                              'rows' => $rows,
                              'empty' => t('No realms defined.')));
}

/**
 * Gives additional options for modifying the user experience.
 */

function openid_sso_provider_additional_options($form, $form_state) {

    // Define additional admin options to improve user interface.
    $form['description'] = array(
      '#markup' => t('<div>Below you will find additonal options that allow you to customize the user experience with the OpenID SSO Provider.</div>'),
    );

    $form['network_name'] = array(
      '#type' => 'fieldset',
      //'#title' => t('Provider Name.'),
      //'#description' => t('Below you will find additonal options that allow you to customize the user experience with OpenID Profile.'),
    );
    $form['network_name']['openid_sso_provider_network_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Single Sign On Provider Name'),
      '#description' => "Set the name of the site that you are using as the Single Sign On Provider or leave it generic.
        <br> This setting exists in the SSO Relying Party configuration as well so make sure you use the same name everywhere to keep this uniform.",
      '#default_value' => variable_get('openid_sso_provider_network_name', "Single-Sign-On-Provider"),
      '#required' => TRUE,
);

    $form['synchronization_options'] = array(
      '#type' => 'fieldset',
      '#title' => t('Synchronization Options'),
      '#description' => t('This area allows you to adjust how data is synced with a OpenID SSO Relying Party when an account is edited on this site.'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['synchronization_options']['openid_sso_provider_account_edit_sync'] = array(
      '#type' => 'radios',
      '#title' => t('User Edit Account Sync'),
      '#default_value' => variable_get('openid_sso_provider_account_edit_sync', "say_nothing"),
      '#options' => array(
            'say_nothing' => t('On an account edit, say nothing.  The user will NOT be notified that remote sites will NOT be updated until their next login.  This is the default.'),
            'notify_user' => t('The user will be notified with a popup that their account info will be synchronized on next login.'),
            'resync_accounts' => t('After the account info has been updated, the user will be redirected back to the relying party site where they will automatically
               be logged out and logged back in.  (Fairly transparently.)  This forces the account to resync on login.'),
       ),
      '#description' => "How do you want this site to act upon the user editing their account page?  Should it attempt to resync the remote account
        with the updates?  Notify the user that they would have to login again for the accounts to be synchronized?  Or say nothing?
        <br> NOTE: This will ONLY resyncronize those fields that CAN be synchronized.  This will ALWAYS include READ ONLY fields,
        but the other fields MAY or MAY NOT synchronize depending upon the settings on your OpenID SSO Relying Party site.
        <br> VERY IMPORTANT!!!!  This will ONLY effect account edits coming FROM OpenID SSO Relying Party websites, not normal local account edits.",
      '#required' => TRUE,
);

    $form['themeing'] = array(
      '#type' => 'fieldset',
      '#title' => t('Themeing Options'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t("Below you will find themeing options that will effect the appearance for your users when using
        the embedded iframes and/or colorbox popups on the OpenID Relying Party websites.  These are the DEFAULT themes used
        in the iframes and colorbox popups.  However, they CAN be OVERRIDEN by using custom theme settings on the individual
        OpenID Relying Party websites.  (Allowing additional themeing per site, if neccessary.)
        <br> VERY IMPORTANT: The themes MUST be installed on THIS site for this to work correctly."),
    );
    $form['themeing']['openid_sso_provider_iframe_theme_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Iframe Theme'),
      '#default_value' => variable_get('openid_sso_provider_iframe_theme_name', 'blank_with_messages'),
      '#description' => t("Enter in the MACHINE NAME of the theme that you want to use for pages displayed inside of iframes.
        <br> NOTE: Ordinarily, this theme would only consist of a message area and the main content area.
        This is done to minimize what is shown in order to blend into the rest of the page."),
      );
    $form['themeing']['openid_sso_provider_colorbox_theme_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Colorbox Theme'),
      '#default_value' => variable_get('openid_sso_provider_colorbox_theme_name', 'blank_with_title_and_messages'),
      '#description' => t("Enter in the MACHINE NAME of the theme that you want to use for pages displayed inside of a colorbox popup.
        <br> NOTE: Ordinarily, this theme would only consist of the page title, a message area and the main content area.
        This is done to only show what is useful to the user in a popup.  (No sidebars, menus, etc.)"),
      );

    $form['openid_sites'] = array(
      '#type' => 'fieldset',
      '#title' => t('Advanced OpenID SSO Provider Options'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      //'#description' => t('Below you will find additonal options that allow you to customize the user experience with OpenID Profile.'),
    );
    $form['openid_sites']['openid_sso_provider_all_sites'] = array(
      '#type' => 'checkbox',
      '#title' => t('Act as an OpenID Provider for ALL sites'),
      '#description' => "Check this box if you would like to allow ANY site to use " . variable_get('site_name') . " as an OpenID Provider.
        <br> Otherwise, ONLY sites you have explicitly listed as trusted will be allowed to authenticate against this site.
        <br> PLEASE NOTE: If the user has already given a site permission to 'auto release' their identity for logins (set for
        automatic login) this will NOT prevent them from continuing to use it.",
      '#default_value' => variable_get('openid_sso_provider_all_sites', 1),
);
    $form['openid_sites']['openid_sso_provider_remove_network_sites_from_openid_list'] = array(
      '#type' => 'checkbox',
      '#title' => t('Remove network sites from the OpenID list'),
      '#description' => "Selecting this box will remove both CORE NETWORK and EXTENDED NETWORK sites from the default
        OpenID sites list.  This is recommended since it can help reduce user confusion that could be caused by having
        duplicate lists of sites.",
      '#default_value' => variable_get('openid_sso_provider_remove_network_sites_from_openid_list', 1),
      '#states' => array(
          // Hide when the OpenID list is not available.
          'invisible' => array(
           ':input[name="openid_sso_provider_all_sites"]' => array('checked' => FALSE),
          ),
        ),
);

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save configuration'),
      '#submit' => array('system_settings_form_submit'),
    );
    $form['reset'] = array(
      '#value' => t('Reset'),
      '#type' => 'submit',
      '#submit' => array('openid_sso_provider_additional_options_reset'),
    );

    return $form;
}

function openid_sso_provider_additional_options_reset($form, &$form_state) {
  variable_del('openid_sso_provider_network_name');
  variable_del('openid_sso_provider_account_edit_sync');
  variable_del('openid_sso_provider_all_sites');
  variable_del('openid_sso_provider_remove_network_sites_from_openid_list');
  variable_del('openid_sso_provider_iframe_theme_name');
  variable_del('openid_sso_provider_colorbox_theme_name');

  menu_rebuild();  // Rebuild the menu cache since the OpenID tab may become active or disabled.

  drupal_set_message(t('Menu cache rebuilt.'));
  drupal_set_message(t('Settings reset.'));

}
