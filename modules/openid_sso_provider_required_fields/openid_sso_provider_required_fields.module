<?php


/**
 *  @file
 */

/**
 * Implements hook_form_alter()
 * Starting point for the logic. All has to be done within different forms.
 */
function openid_sso_provider_required_fields_form_alter(&$form, &$form_state, $form_id) {

  switch($form_id) {

    case 'user_register_form':
      // Figure out realm and define required fields based on that.
      openid_sso_provider_required_fields_register_form_modification($form, $form_state);
      break;

    case 'user_profile_form':
      // Check the realms a user has been logged in from and define the sum of all required fields.
      openid_sso_provider_required_fields_profile_form_modification($form, $form_state);
      break;

    case 'openid_sso_provider_rps_form':
      // Adding settings for required fields on thr relying party settings form.
      openid_sso_provider_required_fields_interface('single', $form, $form_state);
      $form['#submit'][] = 'openid_sso_provider_required_fields_settings_submit';
      break;
  }
}

/**
 * Modifies required field settings on the regitration form based on saved configuration about origin relying party.
 */
function openid_sso_provider_required_fields_register_form_modification(&$form, $form_state) {

  // Check if coming from an relying party
  if (isset($_SESSION['openid_provider'])) {

    // Obtain required field settings
    $required_fields_settings = _openid_sso_provider_required_fields_get_settings($_SESSION['openid_provider']['request']['openid.realm']);
    if (is_array($required_fields_settings)) {
      // Alter the required settings on form
      foreach ($required_fields_settings['user']['standard'] as $field_name => $field_required) {
        if (isset($form[$field_name])) {
          $form[$field_name][$form[$field_name]['#language']][0]['value']['#required'] = $field_required;
        }
      }
    }

    // Alter the required settings on profile2 form
    if (isset($required_fields_settings['profile2'])) {
      foreach ($required_fields_settings['profile2'] as $bundle_name => $fields) {
        if (isset($form['profile_' . $bundle_name])) {
          foreach ($fields as $field_name => $field_required) {
            if (isset($form['profile_' . $bundle_name][$field_name])) {
              $form['profile_' . $bundle_name][$field_name][$form['profile_' . $bundle_name][$field_name]['#language']][0]['value']['#required'] = $field_required;
            }
          }
        }
      }
    }
  }
}

/**
 * Modifies required field settings on the profile form based on saved configuration about from the specific user visited relying parties.
 */
function openid_sso_provider_required_fields_profile_form_modification(&$form, $form_state) {

  global $user;

  // Get list of all the relying parties a user has visited already.
  $required_fields_settings = array();

  // Obtain settings from database to populate default values.
  $result = db_select('openid_provider_relying_party', 'oprp')
  ->condition('oprp.uid', $user->uid, '=')
  ->fields('oprp', array('realm'))
  ->execute();

  foreach ($result as $record) {
    $rp_settings = _openid_sso_provider_required_fields_get_settings($record->realm);
    if (is_array($rp_settings)) {
      $required_fields_settings = array_merge_recursive($required_fields_settings, $rp_settings);
    }
  }

  // Cleaning up the array by merging values.
  foreach ($required_fields_settings as &$profile_type) {
    foreach ($profile_type as &$bundle) {
      foreach ($bundle as $field_name => &$values) {
        if (is_array($values)) {
          $values = (in_array(1, $values)? 1:0);
        }
      }
    }
  }

  if (!empty($required_fields_settings)) {

    // Alter the required settings on form
    foreach ($required_fields_settings['user']['standard'] as $field_name => $field_required) {
      if (isset($form[$field_name])) {
        $form[$field_name][$form[$field_name]['#language']][0]['value']['#required'] = $field_required;
      }
    }
  }

  // Alter the required settings on profile2 form
  if (isset($required_fields_settings['profile2'])) {
    foreach ($required_fields_settings['profile2'] as $bundle_name => $fields) {
      if (isset($form['profile_' . $bundle_name])) {
        foreach ($fields as $field_name => $field_required) {
          if (isset($form['profile_' . $bundle_name][$field_name])) {
            $form['profile_' . $bundle_name][$field_name][$form['profile_' . $bundle_name][$field_name]['#language']][0]['value']['#required'] = $field_required;
          }
        }
      }
    }
  }
}

/**
 *  Adds setting options for required fields based on relying party
 */
function openid_sso_provider_required_fields_interface($type, &$form, $form_state) {

  // Enable/diable checkbox
  $form['required_fields_settings_switch'] = array(
    '#type' => 'checkbox',
    '#title' => t('Override required field setting'),
    '#description' => t('Specify different required profile field definition.'),
  );

  // Wrapping field set.
  $form['required_fields_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Individual required field setting'),
    '#states' => array(
      'visible' => array(
        ':input[name="required_fields_settings_switch"]' => array(
          'checked' => TRUE,
        ),
      ),
    ),
  );

  $instances = field_read_instances(array('entity_type' => 'user', 'bundle' => 'user'));
  $form['required_fields_settings']['user'] = array(
    '#type' => 'fieldset',
    '#title' => t('Account'),
  );

$form['required_fields_settings']['standard'] = array(
    '#type' => 'hidden',
    '#title' => t('Account'),
  );

  foreach ($instances as $instance) {
    $field_info = field_info_field($instance['field_name']);
    $form['required_fields_settings']['user']['standard'][$instance['field_name']] = array(
      '#type' => 'checkbox',
      '#title' => $instance['label'],
      '#default_value' => $instance['required'],
      '#description' => t('Set as required field on registration and user profile form.'),
    );
  }

  // Support Profile2 module
  if (module_exists('profile2')) {

    $instances = field_read_instances(array('entity_type' => 'profile2'));
    $form['required_fields_settings']['profile2'] = array(
      '#type' => 'fieldset',
      '#title' => t('Profile'),
    );
    foreach ($instances as $instance) {
      $field_info = field_info_field($instance['field_name']);

      // Create fieldset depending on profile2 profile type.
      if(!isset($form['required_fields_settings']['profile2'][$instance['bundle']]))  {
        $form['required_fields_settings']['profile2'][$instance['bundle']] = array(
          '#type' => 'fieldset',
          '#title' => $instance['bundle'],
        );
      }
      // Form element to override field's required settings.
      $form['required_fields_settings']['profile2'][$instance['bundle']][$instance['field_name']] = array(
        '#type' => 'checkbox',
        '#title' => $instance['label'],
        '#default_value' => $instance['required'],
        '#description' => t('Set as required field on registration and user profile form.'),
      );
    }
  }

  /** Set default values based on saved settings. **/

  if (isset($form['realm']['#default_value'])) {

    $required_fields_settings = _openid_sso_provider_required_fields_get_settings($form['realm']['#default_value']);
    if (is_array($required_fields_settings)) {
      $form['required_fields_settings_switch']['#default_value'] = 1;
      foreach ($required_fields_settings as $group_type => $values) {
        foreach ($values as $bundle_name => $values) {
          foreach ($values as $field_name => $required) {
            $form['required_fields_settings'][$group_type][$bundle_name][$field_name]['#default_value'] = $required;
          }
        }
      }
    }
  }
}

/**
 *  Submit function saving required fields settings based on relying party
 */
function openid_sso_provider_required_fields_settings_submit($form, &$form_state) {

  // Check if required field settings should be overridden.
  if ($form_state['values']['required_fields_settings_switch']) {

    $required_fields_settings = array();


    $profile_types = array('user');

    // Support Profile2 module.
    if (module_exists('profile2')) {
      $profile_types[] = 'profile2';
    }

    foreach ($profile_types as $profile_type) {

      // Build array of required field settings for one relying party.
      foreach (element_children($form['required_fields_settings'][$profile_type]) as $bundle_name) {
        $required_fields_settings[$profile_type][$bundle_name] = array();
        foreach (element_children($form['required_fields_settings'][$profile_type][$bundle_name]) as $field_name) {
          $required_fields_settings[$profile_type][$bundle_name][$field_name] = $form_state['values'][$field_name];
        }
      }
    }
  }

  else {
    $required_fields_settings = FALSE;
  }

  // Write settings to database.
  db_merge('openid_sso_provider_required_fields')
  ->key(array('realm' => $form_state['values']['realm']))
  ->fields(array('settings' => serialize($required_fields_settings)))
  ->execute();
}

/**
 * Helper function to obtain settings.
 */
function _openid_sso_provider_required_fields_get_settings($realm) {

  // Obtain settings from database to populate default values.
  $result = db_select('openid_sso_provider_required_fields', 'oprf')
  ->condition('oprf.realm', $realm, '=')
  ->fields('oprf', array('realm', 'settings'))
  ->execute();

  foreach ($result as $record) {
    // Setting the default values on form.
    return unserialize($record->settings);
  }
  return FALSE;
}

