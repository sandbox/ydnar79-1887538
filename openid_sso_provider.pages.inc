<?php

/**
 * @file
 * Page callbacks for OpenID SSO Provider.
 */

/**
 * Entity view callback.
 */
function openid_sso_provider_rps_view($entity) {
  drupal_set_title(entity_label('openid_sso_provider_rps', $entity));
  return entity_view('openid_sso_provider_rps', array(entity_id('openid_sso_provider_rps', $entity) => $entity), 'full');
}

/**
 * Page callback for rendering a list of trusted sites.
 */
function openid_sso_provider_relying_parties_opml() {

  // Generate an OPML of trusted sites.
  $output = '<?xml version="1.0" encoding="utf-8"?>' . "\n";
  $output .= '<opml version="2.0">' . "\n";
  $output .= '<head>' . "\n";
  $output .= '  <title>' . t('Trusted relying parties for !site', array('!site' => variable_get('site_name', 'Drupal'))) . '</title>' . "\n";
  $output .= '  <dateCreated>' . format_date(REQUEST_TIME, 'custom', 'r', 0) . '</dateCreated>' . "\n";
  $output .= '</head>' . "\n";
  $output .= '<body>' . "\n";
  foreach (openid_sso_provider_relying_parties() as $rp) {
    $output .= '  <outline text="' . check_plain($rp->name) . '" htmlUrl="' . check_url($rp->realm) . '"/>' . "\n";
  }
  $output .= '</body>' . "\n";
  $output .= '</opml>';

  drupal_add_http_header('Content-Type=text/x-opml');
  print $output;
}

/**
 * Take care of the register page for iframes and colorbox popups.
 *
 */
function openid_sso_provider_register_iframe() {

  global $user;

  if ($user->uid) {
    // User is already logged in, so redirect back to the relying party site and start the login process.
    $clean_realm = check_plain($_GET['realm']);
    $clean_destination = check_plain($_GET['destination']);
    $_GET['destination'] = NULL;  // The destination MUST be removed in order for the drupal_goto to function properly.
     drupal_goto($clean_realm . '/sso/iframe/login?destination=' . $clean_destination);
  } else {
    // If the user is NOT logged in, start the normal registration process.

    if (isset($_GET['openid_sso_theme'])) {  // Custom theme override was passed in the URL.
       $_GET['destination'] = 'empty_page?openid_sso_iframe=TRUE&openid_sso_theme=' . check_plain($_GET['openid_sso_theme']); // Set the new destination.
    } else {  // NO theme override, so use default theme.
       $_GET['destination'] = 'empty_page?openid_sso_iframe=TRUE'; // Set the new destination.
    }

    return drupal_get_form('user_register_form');
  }
}

/**
 * Page callback for Network Sites form where users can see the
 * last time they logged in as well as quick links to the sites.
 *
 * @param object $account User account object for the user.
 */
function openid_sso_provider_network_sites($account) {
  drupal_set_title(check_plain($account->name));

   global $user;

  // Show a modal message when user clicks on "Synchronize now" (there may be a wait).

  $openid_sso_provider_path = drupal_get_path('module', 'openid_sso_provider');
  drupal_add_js(array('openid_sso_provider_remote_sync_wait_message' => t('Please wait while your account is synchronized...')), array('type' => 'setting', 'scope' => JS_DEFAULT));
  drupal_add_js(array('openid_sso_provider_remote_sync_colorbox_uid' => $user->uid), array('type' => 'setting', 'scope' => JS_DEFAULT));
  drupal_add_css("$openid_sso_provider_path/openid_sso_provider_remote_sync.css");
  drupal_add_js("$openid_sso_provider_path/js/jquery.blockUI.js");
  drupal_add_js("$openid_sso_provider_path/js/openid_sso_provider_remote_sync_redirect.js");

  if (module_exists('colorbox') && !isset($_GET['openid_profile_type'])) {  // Enhance the UI if the COLORBOX module is installed AND NOT in a colorbox from a RELYING PARTY site.

      drupal_add_js(array('openid_sso_provider_remote_sync_colorbox' => array(
            'uid' => $user->uid,
            )), 'setting');
       drupal_add_js("$openid_sso_provider_path/js/openid_sso_provider_remote_sync_colorbox.js", array('weight' => 50));
   }

  return drupal_get_form('openid_sso_provider_network_sites_form', $account);
}

/**
 * Form builder function for openid_provider_sites
 */
function openid_sso_provider_network_sites_form($form, $form_state, $user = NULL) {
  if (!$user) {
    global $user;
  }

  module_load_include('inc', 'openid_provider');

    // Show ONLY those sites that are IN THE NETWORK

    $result = db_query("SELECT * FROM {openid_provider_relying_party} LEFT JOIN {openid_sso_provider_rps}
      USING(realm) WHERE openid_provider_relying_party.uid=:uid AND openid_sso_provider_rps.name IS NOT NULL
      AND openid_sso_provider_rps.enabled=1 ORDER BY openid_provider_relying_party.last_time DESC", array(':uid' => $user->uid));

  $form['description'] = array(
      '#type' => 'item',
      '#markup' => t('These are the ' . variable_get('openid_sso_provider_network_name', "Single-Sign-On-Provider") . ' sites that you have linked to this account.
        <br> CORE NETWORK sites will always give you quick and easy login through the ' . variable_get('openid_sso_provider_network_name', "Single-Sign-On-Provider") . ' while
          EXTENDED NETWORK sites are additional websites that we have linked to for your convience.  You can optionally, turn the "Easy login"
          on or off for the EXTENDED NETWORK sites.'),
  );

  $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update'),
  );

  $form['auto_release']['#tree'] = TRUE;
  foreach ($result as $rp) {
      $rps[$rp->rpid] = '';
      $form['site'][$rp->rpid] = array(
          '#markup' => l($rp->name, $rp->realm),  // Change it so that the site name is shown but links to the site.
      );
      $form['last_access'][$rp->rpid] = array(
          '#markup' => $rp->last_time,
      );
      $form['first_time'][$rp->rpid] = array(
          '#markup' => $rp->first_time,
      );
      $form['auto_release'][$rp->rpid] = array(
          '#type' => 'checkbox',
          '#default_value' => $rp->auto_release,
      );

      $form['remote_sync'][$rp->rpid] = array(
          '#type' => 'checkbox',
          '#default_value' => $rp->remote_sync,
      );
      $form['remote_sync_minimum_time'][$rp->rpid] = array(
          '#type' => 'textfield',
          '#default_value' => $rp->remote_sync_minimum_time,
      );
      $form['realm'][$rp->rpid] = array(
          '#type' => 'textfield',
          '#default_value' => $rp->realm,
      );

      // Determine the TYPE of site it is and let the user know.
      $network_type = $rp->extended_network == 0 ? 'CORE NETWORK' : 'EXTENDED NETWORK';

      $form['network_type'][$rp->rpid] = array(
          '#markup' => $network_type,
      );


      // Disable the auto release checkbox if the realm is in the CORE NETWORK.
      if ($rp->extended_network == 0) {
        $form['auto_release'][$rp->rpid]['#disabled'] = TRUE;

        $form['auto_release'][$rp->rpid]['#default_value'] = 1; // Make sure it is checked.
      }
  }

  $form['pager'] = array('#value' => theme('pager', array('tags' => NULL, 'element' => 0)));
  $form['#theme'] = 'openid_sso_provider_network_sites';

  return $form;
}

/**
 * Form submit callback for openid_provider_sites.
 */
function openid_sso_provider_network_sites_form_submit($form, &$form_state) {
  foreach ($form_state['values']['auto_release'] as $key => $value) {
    db_update('openid_provider_relying_party')
      ->fields(array('auto_release' => $value))
      ->condition('rpid', $key)
      ->execute();
  }
  drupal_set_message(t('Settings saved.'));
}

/**
 * Creates a list of "network" sites.
 *
 */
function theme_openid_sso_provider_network_sites($variables) {
  $form = $variables['form'];
  // If there are rows in this form, then $form['title'] contains a list of
  // the title form elements.
  $header = array(t('Easy login'), t('Site'), t('Type of Site'), t('First accessed'), t('Last login / Synchronization'));
  $rows = array();
  if (isset($form['site'])) {  // IF their is any sites listed, cycle through them.
    foreach (element_children($form['site']) as $key) {
      $row = array();
      $row[] = drupal_render($form['auto_release'][$key]);
      $row[] = drupal_render($form['site'][$key]);
      $row[] = drupal_render($form['network_type'][$key]);
      $row[] = format_date(drupal_render(($form['first_time'][$key])));
  }

    global $user;

    if ($form['remote_sync'][$key]['#default_value'] == 1 && //  If the REMOTE synchronization is enabled, add the link when applicable.
        $form['network_type'][$key]['#markup'] == 'CORE NETWORK' &&  //  AND it is a CORE NETWORK site.
        //!isset($_GET['openid_profile_type']) && //  AND this is NOT in a COLORBOX.
        (isset($user->data['openid_sso_last_modified']) && intval($user->data['openid_sso_last_modified']) > intval($form['last_access'][$key]['#markup'])) && //  AND if the profile has been changed since the last login / synchronization.
        ($form['remote_sync_minimum_time'][$key]['#default_value'] == 0 || // AND there is NO minimum time limit
        ((intval($form['remote_sync_minimum_time'][$key]['#default_value']) + intval($form['last_access'][$key]['#markup']) + 5) < time()))) // OR enough time has passed.
      {

          $row[] = format_date(drupal_render(($form['last_access'][$key]))) . ' <br> ' . l(t('Synchronize now'), 'user/' . $user->uid . '/network-sites/sync', array('absolute' => TRUE, 'https' => TRUE, 'query' => array(
                  'realm' => trim($form['realm'][$key]['#default_value'],'/'))));

    } else {    //  Otherwise, just output the normal date information.
        $row[] = format_date(drupal_render(($form['last_access'][$key])));
    }
    $rows[] = $row;
  }
  unset($form['first_time']);
  unset($form['last_access']);
  unset($form['remote_sync']);
  unset($form['remote_sync_minimum_time']);
  unset($form['realm']);
  $output = theme('table', array('header' => $header, 'rows' => $rows));
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }

  $output .= drupal_render_children($form);

  return $output;
}

/**
 * Logout page callback. Logs out and initiates new authentication request if
 * a RP realm is present in $_GET.
 */
function openid_sso_provider_logout_page() {
  openid_sso_provider_logout();
  if ($rp = openid_sso_provider_relying_party($_GET['realm'])) {
    drupal_goto($rp->realm . 'sso/init');
  }
  drupal_goto();
}

/**
 * An Empty Page callback's empty content.
 * This is useful to have as a redirect page after registration or password reset.
 * That way a blank page can be shown with only the message.  (For iframes.)
 *
 * @return string $output
 */
 function openid_sso_provider_page_empty() {
 // Return a space so that an empty page can be used.
 return ' ';
 }

/**
 * Logout form. This form is being visited by an RP after logging out the user.
 * The user is presented with an option to log out on the OP (this site) as
 * well or stay logged in. In both cases the final destination is the RP where
 * the user departed.
 */
function openid_sso_provider_logout_form($form, &$form_state) {
  if ($rp = openid_sso_provider_relying_party($_GET['realm'])) {
    if (user_is_logged_in()) {
      $form = array();
      $form['#rp'] = $rp;
      $form['logout_redirect'] = array(
        '#type' => 'hidden',
        '#value' => $_GET['logout_redirect'],
      );
      $form['message'] = array(
        '#markup' => '<div class="sso-message">' . t('You are logging out of <strong>@relying_party</strong>, would you also like to log out of <strong>@provider</strong>?', array('@relying_party' => $rp->name, '@provider' => variable_get('site_name', 'Drupal'))) . '</div>',
      );
      $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Log out'),
        '#submit' => array('openid_sso_provider_logout_submit'),
      );
      $form['cancel'] = array(
        '#type' => 'submit',
        '#value' => t('Keep me logged in'),
        '#submit' => array('openid_sso_provider_logout_cancel'),
      );
      $form['cancel_completely'] = array(
        '#type' => 'submit',
        '#value' => t('Cancel logout completely'),
        '#submit' => array('openid_sso_provider_logout_cancel_completely'),
      );
      return $form;
    }
    if (isset($_GET['logout_redirect'])) {
      drupal_goto($_GET['logout_redirect']);
    }
    else {
      drupal_goto($rp->realm);
    }
  }
  return array();
}

/**
 * Submit handler for openid_sso_provider_logout_form().
 */
function openid_sso_provider_logout_submit($form, &$form_state) {
  openid_sso_provider_logout();

  // Redirect user to the RP where she came from.
  if (!empty($form_state['values']['logout_redirect'])) {
    drupal_goto($form_state['values']['logout_redirect']);
  }
  else {
    drupal_goto($form['#rp']->realm);
  }
}

/**
 * Submit handler for openid_sso_provider_logout_form().
 */
function openid_sso_provider_logout_cancel($form, &$form_state) {
  // Redirect user to the RP where she came from.
  if (!empty($form_state['values']['logout_redirect'])) {
    drupal_goto($form_state['values']['logout_redirect']);
  }
  else {
    drupal_goto($form['#rp']->realm);
  }
}

/**
 * Submit handler for openid_sso_provider_logout_form().
 */
function openid_sso_provider_logout_cancel_completely($form, &$form_state) {
  // Redirect user to the RP where she came from.
  if (!empty($form_state['values']['logout_redirect'])) {
    if (isset($_GET['openid_sso_colorbox'])) {
        drupal_goto($form_state['values']['logout_redirect'] . '&logout_cancelled=1');

    } else {
        drupal_goto($form_state['values']['logout_redirect'] . '?logout_cancelled=1');

    }
  }
  else {
    drupal_goto($form['#rp']->realm);
  }
}

/**
 * Log out - taken from user_logout(). Difference: no redirect at the end of the
 * function.
 *
 * @see user_logout()
 */
function openid_sso_provider_logout() {
  global $user;

  watchdog('user', 'Session closed for %name.', array('%name' => $user->name));

  module_invoke_all('user_logout', $user);

  // Destroy the current session, and reset $user to the anonymous user.
  session_destroy();
}

/**
 * Redirect the user to the correct site for remote synchronization.  Include a few VERY important pieces
 * of data that the OpenID Relying Party site will need.
 *
 */
function openid_sso_provider_network_sites_sync() {

if (isset($_GET['realm'])) {
  $realm = trim(urldecode($_GET['realm']), '/');

  $rp = openid_sso_provider_relying_party($realm);  // Check if the realm is VALID.

  global $user;

  if ($rp->enabled == 1 && //  Site MUST be ENABLED.
      $rp->remote_sync == 1 && //  If the REMOTE synchronization is enabled, add the link when applicable.
      $rp->extended_network == 0)   //  AND it is a CORE NETWORK site.

  {

    // Check the database to see when the LAST TIME was when the user logged in / synchronized with this site.
    $last_time_result = db_query("SELECT last_time FROM {openid_provider_relying_party}
      WHERE uid=:uid AND realm=:realm", array(':uid' => $user->uid, ':realm' => $rp->realm ))->fetchField();

     if ((isset($user->data['openid_sso_last_modified']) && intval($user->data['openid_sso_last_modified']) > intval($last_time_result)) && //  AND if the profile has been changed since the last login / synchronization.
        ($rp->remote_sync_minimum_time == 0 || // AND there is NO minimum time limit
        ((intval($rp->remote_sync_minimum_time) + intval($last_time_result) + 5) < time()))) // OR enough time has passed.
      {
              $_GET['destination'] = NULL;  // Set destination to NULL to prevent any problems for goto function.

              global $base_url;

              watchdog('openid_sso_relying', 'REMOTE Synchronization with ' . $rp->realm . '  has begun for ' . $user->name);  // Keep a record of the occurence.

          // Set the basic query parameters that will be needed.
          $query_parameters = array('openid_sso_realm' => $base_url,
                              'openid_sso_api' => $rp->api_key,
                              'uid' => $user->uid,
            );

          // Now start doing a few checks and adding additional parameters when needed.

          if ($_GET['q'] == 'user/' . $user->uid . '/network-sites/colorbox/sync') {  // This is in a COLORBOX so add an extra variable to the link.
             $query_parameters['openid_sso_sync_colorbox'] = TRUE;

          }

          //  If this is in a COLORBOX from the RELYING PARTY site, we need to pass some extra variables.
          if (isset($_GET['openid_profile_type'])) {
             $query_parameters['openid_profile_type'] = check_plain($_GET['openid_profile_type']);

              // Check if a custom theme was set.
              if (isset($_GET['openid_profile_theme'])) {
                 $query_parameters['openid_profile_theme'] = check_plain($_GET['openid_profile_theme']);

                    // Check if a custom BLANK theme was set.
                    if (isset($_GET['openid_profile_blank_theme'])) {
                       $query_parameters['openid_profile_blank_theme'] = check_plain($_GET['openid_profile_blank_theme']);
                    }

                }

           }

              drupal_goto($rp->realm . 'sso/synchronize/external/provider', array('absolute' => TRUE, 'https' => TRUE, 
                'query' => array($query_parameters)));  //  Redirect the user to the OpenID Relying Party site for remote synchronization.


     } else {
       drupal_access_denied();
     }


  } else {
    drupal_access_denied();
  }

} else {
  drupal_access_denied();
}
}

/*
 * A simple redirect to send the user back to their NETWORK SITES list after they have been synchronized with a REMOTE network site.
 */

function openid_sso_provider_network_sites_sync_redirect() {

    $_GET['destination'] = NULL;  // Remove the destination parameter to make sure the drupal_goto functions correctly.

    global $user;

    watchdog('openid_sso_relying', 'REMOTE Synchronization with ' . check_plain($_GET['realm']) . '  has finished for ' . $user->name);  // Keep a record of the occurence.

        if (isset($_GET['openid_sso_sync_colorbox']) && module_exists('colorbox')) {  // This is a COLORBOX.
          // Add inline javascript to break any frames and redirect the user to their account page.
          drupal_add_js('top.location.replace("' . url('sso/network-sites/sync/colorbox/redirect', array('absolute' => TRUE, 'https' => TRUE, 'query' => array(
                              'site_name' => check_plain($_GET['site_name']),
                              ))) .'");', 'inline');
          $output = ' ';
          return $output;
        } else {

          $query_parameters = array();

          //  If this is in a COLORBOX from the RELYING PARTY site, the below should be true.
          if (isset($_GET['openid_profile_type'])) {
             $query_parameters['openid_profile_type'] = check_plain($_GET['openid_profile_type']);

              // Check if a custom theme was set.
              if (isset($_GET['openid_profile_theme'])) {
                 $query_parameters['openid_profile_theme'] = check_plain($_GET['openid_profile_theme']);

                    // Check if a custom BLANK theme was set.
                    if (isset($_GET['openid_profile_blank_theme'])) {
                       $query_parameters['openid_profile_blank_theme'] = check_plain($_GET['openid_profile_blank_theme']);
                    }

                }

           }

          drupal_set_message(t('Your account has been successfully synchronized with ') . check_plain($_GET['site_name']));
          drupal_goto('user/' . $user->uid . '/network-sites', array('absolute' => TRUE, 'https' => TRUE, 'query' => array($query_parameters)));  // Redirect user to their account.
        }



}

/*
 * A simple redirect to send the user back to their NETWORK SITES list after they have been synchronized with a REMOTE network site.
 */

function openid_sso_provider_network_sites_sync_colorbox_redirect() {

    $_GET['destination'] = NULL;  // Remove the destination parameter to make sure the drupal_goto functions correctly.

    global $user;

    drupal_set_message(t('Your account has been successfully synchronized with ') . check_plain($_GET['site_name']));
    drupal_goto('user/' . $user->uid . '/network-sites', array('absolute' => TRUE, 'https' => TRUE));  // Redirect user to their account.

}
